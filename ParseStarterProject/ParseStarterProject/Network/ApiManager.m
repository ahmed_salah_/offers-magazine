//
//  ApiManager.m
//  Offers magazine
//
//  Created by Mohammed AL-AWADY on 5/14/17.
//
//

#import <Foundation/Foundation.h>
#import "ApiManager.h"
#import "Magazine.h"
#import "Page.h"
#import "FIRAuth.h"
#import "FIRUser.h"
#import <FirebaseDatabase/FirebaseDatabase.h>

@implementation ApiManager : NSObject

- (void)initialise {
    self.ref = [[FIRDatabase database] reference];

}

+ (instancetype)sharedInstance {
    static ApiManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ApiManager alloc] init];
        [sharedInstance initialise];
    });
    return sharedInstance;
}

- (void)retrieveStoresIntoBlock:(collectionCompletionBlock)storesCompletionCallback {
    [[[self.ref child:@"homePage"] child:@"mainStores"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *_Nonnull snapshot) {
        NSUInteger count = [snapshot childrenCount];
        NSMutableArray *magazineList = [[NSMutableArray alloc] init];
        for (NSUInteger i = 0; i < count; i++) {
            Magazine *magazine = [ApiManager parseSnapshotToMagazine:[snapshot.children.allObjects objectAtIndex:i]];
            if ([magazine visible]) {
                [magazineList addObject:[ApiManager parseSnapshotToMagazine:[snapshot.children.allObjects objectAtIndex:i]]];
            }

        }
        storesCompletionCallback(magazineList);
    }];
}

- (void)postComment:(NSString *)name andComment:(NSString *)comment withCompleteBlock:(void (^)(BOOL isSuccess))block {
    FIRUser *fireUser = [[FIRAuth auth] currentUser];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *currentTime = [dateFormatter stringFromDate:[NSDate date]];
    NSDictionary *messageObject = @{@"comment": comment, @"emailOrPhone": name, @"time": currentTime};
    [[[[[[self.ref child:@"contactUs"] child:@"message"] child:@"iOS"] child:fireUser.uid] childByAutoId] setValue:messageObject withCompletionBlock:^(NSError *error, FIRDatabaseReference *ref) {
        block(!error);
    }];
}

- (void)retrievePagesIntoBlock:(NSString *)pageId withCompletionBlock:(collectionCompletionBlock)pagesCompletionBlock {
    [[[self.ref child:@"Pages"] child:pageId] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *_Nonnull snapshot) {
        NSUInteger count = [snapshot childrenCount];
        NSMutableArray *pageList = [[NSMutableArray alloc] init];
        for (NSUInteger i = 0; i < count; i++) {
            [pageList addObject:[ApiManager parseSnapshotToPage:[snapshot.children.allObjects objectAtIndex:i]]];
        }
        pagesCompletionBlock(pageList);
    }];
}

+ (Magazine *)parseSnapshotToMagazine:(FIRDataSnapshot *)snapshot {

    Magazine *magazine = [[Magazine alloc] init];
    magazine.name = snapshot.value[@"name"];
    magazine.date = snapshot.value[@"date"];
    magazine.logoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@", snapshot.value[@"logoUrl"]]];
    magazine.listID = snapshot.value[@"listID"];
    magazine.label = snapshot.value[@"label"];
    NSInteger visible = [snapshot.value[@"visible"] integerValue];
    magazine.visible = (visible == 1);

    return magazine;
}

+ (Page *)parseSnapshotToPage:(FIRDataSnapshot *)snapshot {

    Page *page = [[Page alloc] init];
    page.pageUrl = [NSString stringWithFormat:@"%@", snapshot.value[@"pageUrl"]];
    page.label = snapshot.value[@"label"];
    return page;
}
@end
