//
//  ApiManager.h
//  Offers magazine
//
//  Created by Mohammed AL-AWADY on 5/14/17.
//
//

#ifndef ApiManager_h
#define ApiManager_h

#import <FirebaseDatabase/FirebaseDatabase.h>
#endif /* ApiManager_h */

typedef void(^collectionCompletionBlock)(NSMutableArray *colletionList);
@interface ApiManager : NSObject

@property (strong, nonatomic) FIRDatabaseReference *ref;

+(instancetype)sharedInstance;

-(void) retrieveStoresIntoBlock: (collectionCompletionBlock) storesCompletionCallback;
-(void) retrievePagesIntoBlock: (NSString*) pageId withCompletionBlock:(collectionCompletionBlock) pagesCompletionBlock;
-(void) postComment:(NSString *)name andComment:(NSString *)comment withCompleteBlock:(void(^)(BOOL isSuccess)) block;
@end
