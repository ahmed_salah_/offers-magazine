//
//  ContactUsViewController.h
//  Offers magazine
//
//  Created by Ahmed Salah on 3/14/16.
//
//

#import <UIKit/UIKit.h>

@interface ContactUsViewController : UIViewController

- (IBAction)rateInAppStore:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *starsImage;

@end
