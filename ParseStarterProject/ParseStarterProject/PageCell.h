//
//  PageCell.h
//  Dynamic Table View Cells
//
//  Created by Jure Zove on 07/06/15.
//  Copyright (c) 2015 Candy Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIView *containerView;

- (void)bind:(NSString *)imageurl;
@end
