//
//  DynamicHeightTableViewController.m
//  Dynamic Table View Cells
//
//  Created by Jure Zove on 07/06/15.
//  Copyright (c) 2015 Candy Code. All rights reserved.
//

#import "DynamicHeightTableViewController.h"
#import "PageCell.h"
#import "IDMPhotoBrowser.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SVProgressHUD.h"
#import "LocalizationSystem.h"
#import "Flurry.h"
#import "ApiManager.h"
#import "EXTScope.h"
#import "Page.h"

#define imageURLResize @"http://images.weserv.nl/?url=%@&w=%@&h=%@&t=fit"

static NSString *PAGE_CELL_ID = @"page_id";

@interface DynamicHeightTableViewController () {
    UIButton *highQualityButton;
    NSMutableArray *rawImageUrls;

}
//@property (nonatomic) NSInteger page;
@property(nonatomic, strong) NSMutableArray *imageHeights;


@end

@implementation DynamicHeightTableViewController {
}

- (void)viewDidLoad {
    [super viewDidLoad];
    rawImageUrls = [NSMutableArray new];
    _imageURLs = [NSMutableArray new];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(reload) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    [Flurry logEvent:@"Native call for onPageView method"];
}

- (void)viewWillAppear:(BOOL)animated {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (self.isFav) {
        self.imageURLs = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Favorites"]];
        self.title = @"Favorites";
        NSString *storeDetailsScreen = @"FavoriteList";
        NSDictionary *dic = NSDictionaryOfVariableBindings(storeDetailsScreen);
        [Flurry logEvent:@"StoreDetails" withParameters:dic];

        if (self.imageURLs.count == 0) {
            [self.tableView setHidden:NO];
            [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.tableView.frame];
            [imageView setImage:[UIImage imageNamed:@"favorite_empty"]];
            [imageView setContentMode:UIViewContentModeCenter];
            [self.tableView addSubview:imageView];
        } else {
            [self setDefaultRowHeights];
            [self.tableView reloadData];
        }
    } else {
        self.title = self.magazine.name;


        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
        UIButton *refresh = [UIButton buttonWithType:UIButtonTypeCustom];
        [refresh setFrame:CGRectMake(70, -2, 44, 44)];
        [refresh setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
        [refresh addTarget:self action:@selector(reload) forControlEvents:UIControlEventTouchUpInside];

        [rightView addSubview:refresh];
        highQualityButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [highQualityButton setFrame:CGRectMake(0, -4, 80, 44)];
        [highQualityButton setTitle:AMLocalizedString(@"High Quality", @"") forState:UIControlStateSelected];
        [highQualityButton setTitle:AMLocalizedString(@"High Quality", @"") forState:UIControlStateNormal];
        [highQualityButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [highQualityButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [highQualityButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
        [highQualityButton addTarget:self action:@selector(setImagesQuality) forControlEvents:UIControlEventTouchUpInside];
        highQualityButton.selected = [[NSUserDefaults standardUserDefaults] boolForKey:@"H.D"];
        //        [rightView addSubview:highQualityButton];//TODO
        UIBarButtonItem *barBtnRight = [[UIBarButtonItem alloc] initWithCustomView:rightView];
        self.navigationItem.rightBarButtonItem = barBtnRight;
    }
    [self reload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Loading photos

- (void)setImagesQuality {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"H.D"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"H.D"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"H.D"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    highQualityButton.selected = [[NSUserDefaults standardUserDefaults] boolForKey:@"H.D"];
    [self reload];
}

- (void)reload {
    [self.refreshControl beginRefreshing];
    @weakify(self);
    [[ApiManager sharedInstance] retrievePagesIntoBlock:self.magazine.listID withCompletionBlock:^(NSMutableArray *colletionList) {
        @strongify(self);
        for (int i = 0; i < colletionList.count; i++) {
            Page *page = colletionList[i];
//                NSString *pageUrl = [page.pageUrl stringByReplacingOccurrencesOfString:@"http://" withString:@""];
//                pageUrl = [pageUrl stringByReplacingOccurrencesOfString:@"https://" withString:@""];
            NSString *imageurl = [self getString:[NSString stringWithFormat:@"%@", page.pageUrl]];
            [rawImageUrls addObject:imageurl];

        }
        [self.refreshControl endRefreshing];
        dispatch_async(dispatch_get_main_queue(), ^{
            for (int i = 0; i < rawImageUrls.count; i++) {

                NSString *originalUrl = rawImageUrls[i];
                [self.imageURLs addObject:originalUrl];
            }
            [self setDefaultRowHeights];
            [self.tableView reloadData];
//
        });
    }];

//    BOOL isHD = [[NSUserDefaults standardUserDefaults] boolForKey:@"H.D"];
//    for (int i = 0; i < rawImageUrls.count; i++) {
//
//        NSString * originalUrl = rawImageUrls[i];
////        NSString *url = isHD?originalUrl :[NSString stringWithFormat:imageURLResize,originalUrl];//TODO
//        [self.imageURLs addObject:originalUrl];
//    }
//
//
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self setDefaultRowHeights];
//        [self.tableView reloadData];
//
//    });


}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.imageURLs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {


    PageCell *cell = [tableView dequeueReusableCellWithIdentifier:PAGE_CELL_ID];
    if (cell == nil) {
        cell = [[PageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:PAGE_CELL_ID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    [SVProgressHUD showWithStatus:AMLocalizedString(@"loading Images", nil)];
    __weak PageCell *weakCell = cell;
    __weak typeof(self) weakSelf = self;

    NSString *imageurl = [self getString:[NSString stringWithFormat:@"%@", _imageURLs[indexPath.row]]];
//    [cell bind:imageurl];
    [cell.mainImageView sd_setImageWithURL:[NSURL URLWithString:imageurl] placeholderImage:[UIImage imageNamed:@"page_placeholder"] options:SDWebImageHandleCookies completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        NSLog(@"URL: %@ and image is: %@", imageURL.absoluteString, image);
        __block UIImage *downloadedImage = image;
        [SVProgressHUD dismiss];
        float originalAspectRatio = downloadedImage.size.width / downloadedImage.size.height;
        float adjustedHeight = [[UIScreen mainScreen] bounds].size.width / originalAspectRatio;
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            // Update table row heights


            weakSelf.imageHeights[(NSUInteger) indexPath.row] = @(adjustedHeight);

            [weakSelf.tableView beginUpdates];
            [weakSelf.tableView endUpdates];
//
//            NSInteger oldHeight = [weakSelf.imageHeights[indexPath.row] integerValue];
//            NSInteger newHeight = (int) image.size.height;
//
//            if (downloadedImage.size.width > CGRectGetWidth(weakCell.mainImageView.bounds)) {
//                CGFloat ratio = downloadedImage.size.height / downloadedImage.size.width;
//                newHeight = CGRectGetWidth(self.view.bounds) * ratio;
//            }
//
//            // Update table row height if image is in different size
//            if (oldHeight != newHeight) {
//                weakSelf.imageHeights[indexPath.row] = @(newHeight);
//
//                [weakSelf.tableView beginUpdates];
//                [weakSelf.tableView endUpdates];
//            }
        });
    }];

    [SVProgressHUD dismiss];


    return cell;
}

- (NSString *)getString:(NSString *)stringUrl {
    NSArray *data = [stringUrl componentsSeparatedByString:@" "];

    for (NSString *str in data) {
        if ([NSURLConnection canHandleRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]])
            return str;
    }
    return stringUrl;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.imageHeights[(NSUInteger) indexPath.row] integerValue];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *storeDetailsScreen = self.isFav ? @"FavoriteList" : self.magazine.name;
    NSString *offerDuration = self.magazine.date;
    NSString *offerUrl = [self.imageURLs objectAtIndex:indexPath.row];
    NSDictionary *dic = self.isFav ? NSDictionaryOfVariableBindings(storeDetailsScreen, offerUrl) : NSDictionaryOfVariableBindings(storeDetailsScreen, offerDuration, offerUrl);
    [Flurry logEvent:@"OfferClick" withParameters:dic];
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:rawImageUrls animatedFromView:tableView];
    browser.displayCounterLabel = YES;
    [self presentViewController:browser animated:YES completion:^{
        [browser setInitialPageIndex:indexPath.row];
    }];
}

#pragma mark - Updating row heights

- (void)setDefaultRowHeights {
    UIImage *placeHolder= [UIImage imageNamed:@"page_placeholder"];
    self.imageHeights = [NSMutableArray arrayWithCapacity:self.imageURLs.count];
    for (int i = 0; i < self.imageURLs.count; i++) {
        self.imageHeights[i] = @(placeHolder.size.height);
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [SVProgressHUD dismiss];
}
@end
