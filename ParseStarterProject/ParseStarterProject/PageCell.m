//
//  PageCell.m
//  Dynamic Table View Cells
//
//  Created by Jure Zove on 07/06/15.
//  Copyright (c) 2015 Candy Code. All rights reserved.
//

#import <SVProgressHUD/SVProgressHUD.h>
#import "PageCell.h"
#import "LocalizationSystem.h"
#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"

@interface PageCell()

@end
//TODO need to be refactored ! CODE DOUPLICATE
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
                green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
                 blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
                alpha:1.0]
@implementation PageCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"PageCell" owner:nil options:nil]lastObject];
        
        
        self.view.layer.masksToBounds=NO;
        self.view.layer.cornerRadius=4;
        self.layer.shadowOpacity = 0.3f;
        self.view.layer.shadowOffset = CGSizeMake(0, 0);
//        self.view.layer.shadowColor = UIColorFromRGB(0xB8B8B8).CGColor;
//        self.view.layer.shadowRadius = 5;
        self.view.layer.borderColor= UIColorFromRGB(0xB8B8B8).CGColor;
        self.view.layer.borderWidth= 0.5f;
        self.containerView.layer.cornerRadius =4;
        self.containerView.layer.masksToBounds=NO;
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)bind:(NSString *)imageurl {
    [SVProgressHUD showWithStatus:AMLocalizedString(@"loading Images", nil) ];



    [self.mainImageView sd_setImageWithURL:[NSURL URLWithString: imageurl]  placeholderImage:[UIImage imageNamed:@"image_placeholder"] options:SDWebImageHandleCookies completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

//        __block UIImage *image2 = image;
//        [SVProgressHUD dismiss];
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [SVProgressHUD dismiss];
//            // Update table row heights
//            NSInteger oldHeight = [weakSelf.imageHeights[indexPath.row] integerValue];
//            NSInteger newHeight = (int)image.size.height;
//
//            if (image2.size.width > CGRectGetWidth(weakCell.mainImageView.bounds)) {
//                CGFloat ratio = image2.size.height / image2.size.width;
//                newHeight = CGRectGetWidth(self.view.bounds) * ratio;
//            }
//
//            // Update table row height if image is in different size
//            if (oldHeight != newHeight) {
//                weakSelf.imageHeights[indexPath.row] = @(newHeight);
//
//                [weakSelf.tableView beginUpdates];
//                [weakSelf.tableView endUpdates];
//            }
//        });
    }];

    [SVProgressHUD dismiss];

}
@end
