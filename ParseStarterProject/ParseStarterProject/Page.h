//
//  Page.h
//  Offers magazine
//
//  Created by Mohammed AL-AWADY on 5/14/17.
//
//

#ifndef Page_h
#define Page_h


#endif /* Page_h */
@interface Page : NSObject

@property  NSString* pageUrl;
@property  NSString* label;

@end
