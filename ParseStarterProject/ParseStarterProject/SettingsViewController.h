//
//  DetailsViewController.h
//  Offers magazine
//
//  Created by Ahmed Salah on 3/14/16.
//
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
