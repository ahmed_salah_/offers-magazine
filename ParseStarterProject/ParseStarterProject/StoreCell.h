//
//  StoreCell.h
//  Offers magazine
//
//  Created by Ahmed Salah on 3/11/16.
//
//

#import <UIKit/UIKit.h>

@class Magazine;

@interface StoreCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;

- (void)bind:(Magazine *)magazine;
@end
