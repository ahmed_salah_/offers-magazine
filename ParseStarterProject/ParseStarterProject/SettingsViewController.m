//
//  DetailsViewController.m
//  Offers magazine
//
//  Created by Ahmed Salah on 3/14/16.
//
//

#import "SettingsViewController.h"
#import "PerefrencesCell.h"
#import "CZPickerView.h"
#import "OffersMagazineAppDelegate.h"
#import "LocalizationSystem.h"
#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

@interface SettingsViewController ()<PerefrencesCellDelegate, CZPickerViewDataSource, CZPickerViewDelegate>
{
    NSString *lang ;
}
@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    lang = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *vu = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.tableView.frame.size.width, 100)];
    [vu setBackgroundColor:[UIColor colorWithRed:196.f/255.f green:218.f/255.f blue:231.f/255.f alpha:1]];
    [vu setTextColor:[UIColor blackColor]];
    if ([lang isEqualToString:@"ar"]) {
        [vu setTextAlignment:NSTextAlignmentRight];

    }else{
        [vu setTextAlignment:NSTextAlignmentLeft];

    }
    [vu setFont:[UIFont systemFontOfSize:15]];
    NSString *titleSection = AMLocalizedString(@"Settings", nil);
    [vu  setText:[NSString stringWithFormat:@"%@",titleSection]];
    
    return vu;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 30;
}
#pragma tableView delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 2) {
        CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:AMLocalizedString(@"Select Language",nil)cancelButtonTitle:AMLocalizedString(@"Cancel",nil) confirmButtonTitle:AMLocalizedString(@"Confirm",nil)];
        picker.delegate = self;
        picker.dataSource = self;
        [picker show];
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier1 = @"Cell";
    PerefrencesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    if (cell == nil) {
        cell = [[PerefrencesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] ;
        CGRect switchFrame = cell.switchControl.frame;
        CGRect titleFrame = cell.titleLabel.frame;
        titleFrame.origin.x = [[UIScreen mainScreen ] bounds].size.width-300;
        if ([lang isEqualToString:@"ar"]) {
            if ( IDIOM == IPAD ) {
                switchFrame.origin.x = -300;
                switchFrame.size.width = 51;

            } else {
                switchFrame.origin.x = cell.titleLabel.frame.origin.x;
                switchFrame.size.width = 51;
            }
            [cell.titleLabel setFrame:titleFrame];
            [cell.switchControl setFrame:switchFrame];
            [cell.titleLabel setTextAlignment:NSTextAlignmentRight];
        }
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.delegate = self;
    cell.backgroundColor = [UIColor clearColor];
   
    if (indexPath.row == 0) {
        cell.titleLabel.text = AMLocalizedString(@"New Offers Update Notifications", nil) ;

    }else if(indexPath.row == 1){
        cell.titleLabel.text = AMLocalizedString(@"Images Quality H.D", nil);
        [cell.switchControl setOn:[[NSUserDefaults standardUserDefaults]boolForKey:@"H.D"]];
   
    }else{
        cell.switchControl.hidden = YES;
        cell.titleLabel.text = AMLocalizedString( @"Language", nil);
    }
    
    return cell;
}
-(void)didTapSwich:(PerefrencesCell *)cell{
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    
    OffersMagazineAppDelegate *ApplicationDelegate = (OffersMagazineAppDelegate *)[UIApplication sharedApplication].delegate;
    if (index.row == 0) {
        if (cell.switchControl.isOn) {
            [ApplicationDelegate subscribeNotifications];
        }else{
            [ApplicationDelegate unsubscribeNotifications];
        }
    }else{
        if (cell.switchControl.isOn) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"H.D"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"H.D"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
   
}

#pragma mark - CZPickerViewDataSource


/* number of items for picker */
- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView{
    return 2;
}



/* picker item title for each row */
- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    if (row == 0) {
        return @"English";
    }else{
        return @"العربيه";
    }
    
}
#pragma mark - CZPickerViewDelegate

- (void)czpickerView:(CZPickerView *)pickerView
didConfirmWithItemAtRow:(NSInteger)row{
    if (row == 0) {
        LocalizationSetLanguage(@"en");
        [[NSUserDefaults standardUserDefaults]setObject:@"en" forKey:@"language"];
    }else{
        LocalizationSetLanguage(@"ar");
        [[NSUserDefaults standardUserDefaults]setObject:@"ar" forKey:@"language"];
    }
    lang = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

    [self.tableView reloadData];
}

@end
