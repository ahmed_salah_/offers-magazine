//
//  StoreCell.m
//  Offers magazine
//
//  Created by Ahmed Salah on 3/11/16.
//
//

#import "StoreCell.h"
#import "Magazine.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
                green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
                 blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
                alpha:1.0]

@implementation StoreCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.layer.cornerRadius = 6;
    self.layer.masksToBounds = NO;
    self.layer.borderColor = UIColorFromRGB(0xB8B8B8).CGColor;
    self.layer.borderWidth = 0.5f;
    self.layer.shadowColor = UIColorFromRGB(0xB8B8B8).CGColor;
    self.layer.shadowOpacity = 0.3f;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowRadius = 5;
    self.containerView.layer.cornerRadius = 6;
    self.containerView.layer.masksToBounds = YES;
}

- (void)bind:(Magazine *)magazine {
    [self.imageView sd_setImageWithURL:magazine.logoUrl placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    self.titleLabel.text = magazine.date;
}


@end
