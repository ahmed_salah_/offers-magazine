//
//  PerefrencesCell.h
//  OredooBuzz
//
//  Created by Ahmed Salah on 10/8/15.
//  Copyright © 2015 Ahmed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PerefrencesCell;
@protocol PerefrencesCellDelegate
- (void) didTapSwich:(PerefrencesCell *)cell ;
@end
@interface PerefrencesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
- (IBAction)turnOffAndOn:(id)sender;
@property (weak, nonatomic) id<PerefrencesCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISwitch *switchControl;

@end
