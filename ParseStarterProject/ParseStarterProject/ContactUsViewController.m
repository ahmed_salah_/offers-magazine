//
//  ContactUsViewController.m
//  Offers magazine
//
//  Created by Ahmed Salah on 3/14/16.
//
//

#import "ContactUsViewController.h"
#import "LocalizationSystem.h"
#import "SVProgressHUD.h"
#import "ApiManager.h"

@interface ContactUsViewController () {
    CGRect original;
}
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UITextField *mailTextField;
@property(weak, nonatomic) IBOutlet UITextView *feedBackTextView;
@property(weak, nonatomic) IBOutlet UILabel *fbTitle;
@property(weak, nonatomic) IBOutlet UILabel *twitterTitle;
@property(weak, nonatomic) IBOutlet UILabel *shareTitle;
@property(weak, nonatomic) IBOutlet UILabel *rateTitle;
@property(weak, nonatomic) IBOutlet UIButton *sendBtn;
@end

@implementation ContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"]) {

        [self.mailTextField setTextAlignment:NSTextAlignmentRight];
        [self.feedBackTextView setTextAlignment:NSTextAlignmentRight];

    } else {
        [self.mailTextField setTextAlignment:NSTextAlignmentLeft];
        [self.feedBackTextView setTextAlignment:NSTextAlignmentLeft];
    }
    self.feedBackTextView.text = AMLocalizedString(@"Comment, Suggestion,Complaint...", nil);
    self.mailTextField.textColor = [UIColor lightGrayColor];
    self.mailTextField.layer.cornerRadius = 4;
    self.mailTextField.layer.masksToBounds = YES;
    self.mailTextField.layer.borderColor = [[UIColor colorWithRed:229.f / 255.f green:229.f / 255.f blue:229.f / 255.f alpha:1] CGColor];
    self.mailTextField.layer.borderWidth = 1.0f;


    self.feedBackTextView.textColor = [UIColor lightGrayColor];
    self.feedBackTextView.layer.cornerRadius = 4;
    self.feedBackTextView.layer.masksToBounds = YES;
    self.feedBackTextView.layer.borderColor = [[UIColor colorWithRed:229.f / 255.f green:229.f / 255.f blue:229.f / 255.f alpha:1] CGColor];
    self.feedBackTextView.layer.borderWidth = 1.0f;
    self.title = AMLocalizedString(@"Share Your Love", nil);
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    self.titleLabel.text = AMLocalizedString(@"comment", nil);
    self.fbTitle.text = AMLocalizedString(@"facebook", nil);
    self.twitterTitle.text = AMLocalizedString(@"twitter", nil);
    self.shareTitle.text = AMLocalizedString(@"share", nil);
    self.rateTitle.text = AMLocalizedString(@"appStore", nil);
    [self.sendBtn setTitle:AMLocalizedString(@"send", nil) forState:UIControlStateNormal];
    [self.mailTextField setPlaceholder:AMLocalizedString(@"email", nil)];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rateInAppStore:)];
    tapGesture.numberOfTapsRequired = 1;
    [_starsImage addGestureRecognizer:tapGesture];
}

- (void)handleTapFrom:(UITapGestureRecognizer *)recognizer {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)share:(id)sender {

    NSURL *myWebsite = [NSURL URLWithString:@"http://www.OffersMagazine.com/"];

    NSArray *objectsToShare = @[myWebsite];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];

    NSArray *excludeActivities = @[UIActivityTypePostToFacebook,
            UIActivityTypePostToTwitter,
            UIActivityTypePostToWeibo,
            UIActivityTypeMessage,
            UIActivityTypeMail,
            UIActivityTypePrint,
            UIActivityTypeCopyToPasteboard,
            UIActivityTypeAssignToContact,
            UIActivityTypeSaveToCameraRoll,
            UIActivityTypeAddToReadingList,
            UIActivityTypePostToFlickr,
            UIActivityTypePostToVimeo,
            UIActivityTypePostToTencentWeibo,
            UIActivityTypeAirDrop,
            UIActivityTypeOpenInIBooks];

    activityVC.excludedActivityTypes = excludeActivities;

    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)facebookLike:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/offersMagazineApp/"]];

}

- (IBAction)twitterLike:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/OffersMagazine"]];
}

- (IBAction)sendFeedBack:(id)sender {
    [SVProgressHUD showWithStatus:AMLocalizedString(@"send feedBack", nil)];
    if([[self.feedBackTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0 ){
        [SVProgressHUD showErrorWithStatus:AMLocalizedString(@"feedbackEmpty",nil)];
        return;
    }
    [self.view endEditing:YES];
//
//    NSObject *newGroup = [NSObject objectWithClassName:@"ContactUs"];
//    newGroup[@"Email"] = self.mailTextField.text;
//    newGroup[@"Message"] = self.feedBackTextView.text;
//    [newGroup saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
//    {
//        if(error){
//            [SVProgressHUD showErrorWithStatus:AMLocalizedString(@"feedBackError", nil)];
//        }else{
//            [SVProgressHUD showSuccessWithStatus:AMLocalizedString(@"feedBackSent", nil)];
//        }
//    }];
    [[ApiManager sharedInstance] postComment:self.mailTextField.text andComment:self.feedBackTextView.text withCompleteBlock:^(BOOL isSuccess) {
        if (isSuccess) {
            [SVProgressHUD showSuccessWithStatus:AMLocalizedString(@"feedBackSent", nil)];
            self.mailTextField.text =@"";
            self.feedBackTextView.text=@"";
        } else {
            [SVProgressHUD showErrorWithStatus:AMLocalizedString(@"feedBackError", nil)];
        }
    }
    ];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    CGRect rect = self.view.frame;
    original = self.view.frame;

    rect.origin.y = -150;
    [UIView animateWithDuration:0.25 animations:^{
        [self.view setFrame:rect];
    }];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:AMLocalizedString(@"Comment, Suggestion,Complaint...", nil)]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    } else {

    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text = AMLocalizedString(@"Comment, Suggestion,Complaint...", nil);
        textView.textColor = [UIColor lightGrayColor]; //optional
    }

    [UIView animateWithDuration:0.25 animations:^{

        [self.view setFrame:original];
    }];

    [textView resignFirstResponder];
}

- (IBAction)rateInAppStore:(id)sender {
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=<1095025990>&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8"]];


//    NSString *appName = [NSString stringWithString:[[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"]];
    NSURL *appStoreURL = [NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1095025990"];


//    NSURL *appStoreURL = [NSURL URLWithString:[NSString stringWithFormat:@"http:/itunes.com/app/%@",[appName stringByReplacingOccurrencesOfString:@" " withString:@""]]];


    [[UIApplication sharedApplication] openURL:appStoreURL];

}
@end
