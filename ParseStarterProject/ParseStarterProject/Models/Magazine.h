//
//  Magazine.h
//  Offers magazine
//
//  Created by Mohammed AL-AWADY on 5/14/17.
//
//

#ifndef Magazine_h
#define Magazine_h


#endif /* Magazine_h */
@interface Magazine : NSObject

@property (strong) NSString* name;
@property  (strong) NSURL* logoUrl;
@property  (strong) NSString* listID;
@property (strong) NSString* label;
@property  (strong) NSString* date;
@property  BOOL visible;

@end
