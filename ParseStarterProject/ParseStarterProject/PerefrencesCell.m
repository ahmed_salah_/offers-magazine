//
//  PerefrencesCell.m
//  OredooBuzz
//
//  Created by Ahmed Salah on 10/8/15.
//  Copyright © 2015 Ahmed Salah. All rights reserved.
//

#import "PerefrencesCell.h"

@implementation PerefrencesCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"PerefrencesCell" owner:nil options:nil]                lastObject];

    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)turnOffAndOn:(id)sender {
    [self.delegate didTapSwich:self];
}
@end
