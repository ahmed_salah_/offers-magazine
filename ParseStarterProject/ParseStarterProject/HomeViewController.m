/**
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "HomeViewController.h"


#import "StoreCell.h"
#import "DynamicHeightTableViewController.h"
#import "SettingsViewController.h"
#import "ContactUsViewController.h"
#import "LocalizationSystem.h"
#import "Flurry.h"
#import "Magazine.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Magazine.h"
#import "ApiManager.h"
#import "EXTScope.h"

#define currentLanguageBundle [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[[NSLocale preferredLanguages] objectAtIndex:0] ofType:@"lproj"]]

static NSString *STORE_CELL_NIB = @"StoreCell";
static NSString *STORE_CELL_ID = @"Cell";
static NSString *SETTINGS_CONTROLLER_NIB = @"SettingsViewController";

@interface HomeViewController () {
    UILabel *title;
}
@end

@implementation HomeViewController {
}

#pragma mark -
#pragma mark UIViewController

- (void)loadData {

    [self.refreshControl beginRefreshing];

    @weakify(self);
    [[ApiManager sharedInstance] retrieveStoresIntoBlock:^(NSMutableArray *stores) {

        data = [stores copy];
        @strongify(self);
        [self.collectionView reloadData];
        [self.refreshControl endRefreshing];
    }];
}


- (void)goToSettings {
    SettingsViewController *settingsVC = [[SettingsViewController alloc] initWithNibName:SETTINGS_CONTROLLER_NIB bundle:nil];
    [self.navigationController pushViewController:settingsVC animated:YES];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView registerNib:[UINib nibWithNibName:STORE_CELL_NIB bundle:nil]
          forCellWithReuseIdentifier:STORE_CELL_ID];

//    [self.collectionView registerNib:[UINib nibWithNibName:@"FooterCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];

    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(loadData) forControlEvents:UIControlEventValueChanged];
    [self.collectionView setRefreshControl:self.refreshControl];
    [self loadData];

    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 180, 30)];

    UIButton *refresh = [UIButton buttonWithType:UIButtonTypeCustom];
    [refresh setFrame:CGRectMake(100, -6, 44, 44)];
    [refresh setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    [refresh addTarget:self action:@selector(refreshBtn) forControlEvents:UIControlEventTouchUpInside];

    [rightView addSubview:refresh];

    UIButton *settings = [UIButton buttonWithType:UIButtonTypeCustom];
    [settings setFrame:CGRectMake(130, -6, 44, 44)];
    [settings setImage:[UIImage imageNamed:@"settings"] forState:UIControlStateNormal];
    [settings addTarget:self action:@selector(goToSettings) forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:settings];


    UIButton *favorite = [UIButton buttonWithType:UIButtonTypeCustom];
    [favorite setFrame:CGRectMake(160, -6, 44, 44)];
    [favorite setImage:[UIImage imageNamed:@"favorite"] forState:UIControlStateNormal];
    [favorite addTarget:self action:@selector(goToFavorites) forControlEvents:UIControlEventTouchUpInside];

//    [rightView addSubview:favorite];//TODO:
    UIBarButtonItem *barBtnRight = [[UIBarButtonItem alloc] initWithCustomView:rightView];
    self.navigationItem.rightBarButtonItem = barBtnRight;

    UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    title = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 150, 30)];

    [myView setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth];
    [title setTextColor:[UIColor whiteColor]];
    [title setFont:[UIFont boldSystemFontOfSize:20.0]];
    [title setAdjustsFontSizeToFitWidth:YES];
    [title setBackgroundColor:[UIColor clearColor]];
    UIImage *image = [UIImage imageNamed:@"logo_splash"];
    UIImageView *myImageView = [[UIImageView alloc] initWithImage:image];

    myImageView.frame = CGRectMake(0, 0, 30, 30);
    myImageView.layer.cornerRadius = 5.0;
    myImageView.layer.masksToBounds = YES;
    myImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    myImageView.layer.borderWidth = 0.1;

    [myView addSubview:title];
    [myView setBackgroundColor:[UIColor clearColor]];
    [myView addSubview:myImageView];
    self.navigationItem.titleView = myView;
    ////

    [Flurry logEvent:@"Native call for onPageView method"];

}

- (void)refreshBtn {
    [Flurry logEvent:@"Refresh"];
    [self loadData];
}

- (void)goToFavorites {
    DynamicHeightTableViewController *table = [[DynamicHeightTableViewController alloc] init];
    table.isFav = YES;
    [self.navigationController pushViewController:table animated:YES];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

- (void)viewWillAppear:(BOOL)animated {
    title.text = AMLocalizedString(@"Offers Magazine", @"Offers Magazine");
    [self.collectionView reloadData];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return data.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    StoreCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:STORE_CELL_ID forIndexPath:indexPath];


    Magazine *model = data[(NSUInteger) indexPath.row];
    [cell bind:model];


    return cell;
}
//-(BOOL) collectionView:(UICollectionView*) collectionView shouldHighlightItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
//    return NO;
//}
//- (UICollectionReusableView *)collectionView:(UICollectionView *)theCollectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)theIndexPath
//{
//
//    FooterCollectionReusableView *theView;
//
//    if(kind == UICollectionElementKindSectionFooter)
//    {
//        theView = [theCollectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer" forIndexPath:theIndexPath];
//        [theView setDelegate:self];
//    }
//    theView.TitleLabel.text = AMLocalizedString(@"title text", nil);
//    [theView.ContactUsBtn setTitle:AMLocalizedString(@"contact US", nil) forState:UIControlStateNormal];
//    return theView;
//}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Select Item
    DynamicHeightTableViewController *table = [[DynamicHeightTableViewController alloc] init];
    Magazine *object = [data objectAtIndex:indexPath.row];
    table.magazine = object;
    [self.navigationController pushViewController:table animated:YES];
//    [object fetchImagesAssetsWithCompletionBlock:^(NSArray *assets, NSError *error) {
//        @try {
//            table.imageURLs = [NSMutableArray arrayWithArray:assets];
//
//
//        } @catch (NSException *exception) {
//
//        } @finally {
//
//        }
//            }];
//
//    table.storeName = object.offersDuration;
//    table.titleName = object.name;
//    table.offerDuration = object.offersDuration;
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:object.date, object.name, nil];
    [Flurry logEvent:@"StoreDetails" withParameters:dic];

}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([[UIScreen mainScreen] bounds].size.width == 320) {
        return CGSizeMake(140, 118);

    }
    return CGSizeMake(170, 118);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    CGFloat left = (section)%2 == 0? 20:10;//to assign the value 20 to the left coulmn and 10 to the right one
    CGFloat right = (section)%2 == 0? 20:10;//to access the right coulomn
    return UIEdgeInsetsMake(20, left, 20, right);
}

- (IBAction)onContactUsClicked:(id)sender {
    ContactUsViewController *contactUS = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
    [self.navigationController pushViewController:contactUS animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(320.0f, 111.0f);
}


@end
