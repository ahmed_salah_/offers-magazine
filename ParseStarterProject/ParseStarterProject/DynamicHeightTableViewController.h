//
//  DynamicHeightTableViewController.h
//  Dynamic Table View Cells
//
//  Created by Jure Zove on 07/06/15.
//  Copyright (c) 2015 Candy Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Magazine.h"

@interface DynamicHeightTableViewController : UITableViewController
@property (nonatomic, assign) Magazine *magazine;
@property (nonatomic, strong) NSMutableArray *imageURLs;
@property (nonatomic, assign) BOOL isFav;
@property (nonatomic, assign) NSString *listName;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@end
